call gradlew shadowJar -x test
bash -c "scp "$(pwd)/build/libs/deployer.jar" root@ssh.leandi.net:/root/deployer/deployer.jar"
bash -c "ssh root@ssh.leandi.net sh /root/deployer/restart.sh"
