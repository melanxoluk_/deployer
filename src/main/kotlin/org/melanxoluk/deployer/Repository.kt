package org.melanxoluk.deployer

import com.typesafe.config.ConfigFactory
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Project
import java.io.File
import java.nio.file.Paths


class Repository {
    val gitlabServices = mutableMapOf<String, GitlabService>()
    val telegramChannels = mutableMapOf<String, TelegramChannel>()
    val projects = mutableMapOf<String, DeployerProject>()

    fun reload() {
        gitlabServices.clear()
        telegramChannels.clear()
        projects.clear()

        val config = ConfigFactory.parseFile(File("deployer.conf"))
        config.getObjectList("gitlab").forEach { obj ->
            val conf = obj.toConfig()
            val name = conf.getString("name")
            gitlabServices[name] =
                GitlabService(
                    name,
                    conf.getString("host"),
                    conf.getString("token")
                )
        }

        config.getObjectList("telegram").forEach { obj ->
            val conf = obj.toConfig()
            val name = conf.getString("name")
            telegramChannels[name] =
                TelegramChannel(
                    name,
                    conf.getString("token"),
                    conf.getString("channel")
                )
        }

        config.getObjectList("projects").forEach { obj ->
            val conf = obj.toConfig()
            val name = conf.getString("name")
            val port = conf.getInt("port")
            val path = Paths.get(conf.getString("path"))
            val key = conf.getString("key")
            val gitlabName = conf.getString("project_name")
            val gitlabService = gitlabServices[conf.getString("gitlab")]!!
            val gitlabProject = getProject(gitlabName, gitlabService)
            val telegram = telegramChannels[conf.getString("telegram")]!!
            projects[key] =
                DeployerProject(name, port, path, key, gitlabService, telegram, gitlabProject)
        }
    }

    private fun getProject(name: String, gitlabService: GitlabService): Project {
        val gitlabApi = GitLabApi(gitlabService.host, gitlabService.token)
        return gitlabApi.projectApi.getProject(name)
    }
}
