package org.melanxoluk.deployer

import org.gitlab4j.api.models.Project
import java.nio.file.Path


data class GitlabService(
    val name: String,
    val host: String,
    val token: String)

data class DeployerProject(
    val name: String,
    val port: Int,
    val path: Path,
    val key: String,
    val gitlab: GitlabService,
    val telegram: TelegramChannel,
    val project: Project)

data class TelegramChannel(
    val name: String,
    val token: String,
    val channel: String)
