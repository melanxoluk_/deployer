package org.melanxoluk.deployer

import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.application.call
import io.ktor.application.log
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.JobStatus
import org.gitlab4j.api.utils.JacksonJson
import org.gitlab4j.api.webhook.Event
import org.gitlab4j.api.webhook.PipelineEvent
import org.slf4j.LoggerFactory
import java.nio.file.Files


private val version = "check redeploy"
private val log = LoggerFactory.getLogger("deployer")

fun main(args: Array<String>) {
    val repo = Repository().apply { reload() }
    val telegram = Telegram()
    val gitlabEventsMapper = JacksonJson().objectMapper.registerKotlinModule()
    val deployerProject = repo.projects.values.first { it.name == "deployer" }

    embeddedServer(Netty, applicationEngineEnvironment {
        connector {
            host = "0.0.0.0"
            port = 11001
        }

        module {
            routing {
                get("/ping") {
                    call.respond("pong")
                }

                get("/reload") {
                    repo.reload()
                    call.respond("reloaded")
                }

                post("/webhooks") {
                    val queries = call.request.queryParameters
                    val key = queries["key"]
                    if (key == null) {
                        log.error("not found key query parameter")
                        return@post
                    }

                    val project = repo.projects[key]
                    if (project == null) {
                        log.error("deployer project null")
                        return@post
                    }

                    val event = gitlabEventsMapper.readValue(call.receiveText(), Event::class.java)
                    if (event.objectKind == PipelineEvent.OBJECT_KIND) {
                        val pipeline = event as PipelineEvent
                        val status = pipeline.objectAttributes.status
                        if (status == "success") {
                            log.info("start to deploy project ${project.name}")

                            val artifactBytes =
                                pipelineArtifact(
                                    project,
                                    pipeline.objectAttributes.id)
                            log.info("artifact bytes are downloaded")

                            val path = project.path.resolve("artifacts.zip")
                            Files.write(path, artifactBytes)
                            log.info("artifacts.zip downloaded to $path")

                            // deployer should update itself on background
                            if (project.name == "deployer") {
                                deployDeployer(project)
                            // while any other could run in foreground
                            } else {
                                if (project.port == 0) {
                                    deployStatic(project)
                                } else {
                                    deployProject(project)
                                }
                            }
                            log.info("project ${project.name} deployed")

                            telegram.sendMessage(project.telegram, "deploy swap started for **${project.name}**")
                        }

                        log.info("${pipeline.project.name} $status pipeline ${pipeline.objectAttributes.id} processed")
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }
    }).start()

    log.info("deployer started: $version")
    telegram.sendMessage(deployerProject.telegram, "deployer started: $version")
}

private fun deployDeployer(project: DeployerProject) {
    log.info("self deploy started")

    ProcessBuilder(
        "nohup",
        "sh",
        "/root/deployer/swap.sh",
        project.path.toAbsolutePath().toString(),
        project.name,
        project.port.toString()
    ).start()
}

private fun deployProject(project: DeployerProject) {
    val swapExitValue =
        ProcessBuilder(
            "sh",
            "/root/deployer/swap.sh",
            project.path.toAbsolutePath().toString(),
            project.name,
            project.port.toString()
        ).start().run {
            waitFor()
            exitValue()
        }

    log.info("deploy finished with code $swapExitValue")
}

private fun deployStatic(project: DeployerProject) {
    val swapExitValue =
        ProcessBuilder(
            "sh",
            "/root/deployer/static_swap.sh",
            project.path.toAbsolutePath().toString(),
            project.name
        ).start().run {
            waitFor()
            exitValue()
        }

    log.info("deploy static finished with code $swapExitValue")
}

private fun pipelineArtifact(project: DeployerProject, pipelineId: Int): ByteArray? {
    val api = project.gitlab.run { GitLabApi(host, token) }
    val gitlabProjectId = project.project.id

    val jobs = api.jobApi.getJobsForPipeline(gitlabProjectId, pipelineId)

    val artifactSuccessJobs = jobs.firstOrNull {
        it.status == JobStatus.SUCCESS && it.artifactsFile != null
    }

    if (artifactSuccessJobs == null) {
        log.error("not found success job with artifact in pipeline $pipelineId on project ${project.name}")
        return null
    }

    return api.jobApi
        .downloadArtifactsFile(gitlabProjectId, artifactSuccessJobs.id)
        .readBytes()
}
